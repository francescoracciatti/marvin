# Marvin

Marvin is the paranoid android of *The Hitchhiker's Guide to the Galaxy*.

Marvin is also a tool that bundles the abstract modeler [UPPAAL](http://www.uppaal.org/) with the attack simulator 
[ASF](xxx).
In detail, Marvin produces a complete network/application model to be run on ASF, 
starting from an abstract UPPAAL model. 

Therefore, by using Marvin you can:
 * assess the concrete behavior of protocols and applications modeled via UPPAAL, on attack free scenarios;
 * evaluate the concrete effects of attacks, which can be described via the attack language provided by ASF; 
 * refine the initial UPPAAL model thanks to the insights provided by ASF. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes. In the section [Deployment](#deployment) below, you can find notes on how to deploy the resulting 
network model on a live system. 
The section [Built With](#built-with) below, lists the version numbers of the tools used to develop the project and deploy the 
resulting network model. 

### Prerequisites

The project is written in Python3. It was developed on macOS High Sierra via PyCharm, and it is 
designed for running on Ubuntu 18.04.

Since the project is written in Python3, it is not mandatory to use any IDE, 
and it can be run on any OS provided with a Python3 interpreter 3.8.3 or greater.

### Installing

Install the Python interpreter 3.8.3 or greater.
 Download the project and import it into PyCharm. 
Then, in the project's Run/Debug Configuration, set:
 * the Python interpreter with an interpreter of version 3.8.3 or greater;
 * the Script Path to `../marvin/src/marvin.py` (which contains the `main` function);
 * the Working directory to `../marvin/src`.

Take care of using venv for enforcing system isolation.
The file requirements.txt lists the dependencies on which the project relies on.
PyCharm will import them automatically.
If in doubt, refer to the [PyCharm documentation](https://www.jetbrains.com/pycharm/documentation/).

### Running the tests

Tests are made via [unittest framework](https://docs.python.org/3/library/unittest.html). 
After installing the project on your development machine, you can run the tests.
All tests must pass.

Before running any simulation, make sure that the vanilla simulator, produced by tests, works 
as expected on the live system (see section [Deployment](#deployment) below). 
After running tests, you will find the vanilla simulator in the folder: 
`../marvin/model/output/asf/*-vanilla-asf`.

### Usage

For generating an ASF model starting from an UPPAAL model you have to:
 1. put the xml file describing your UPPAAL model into the folder `../marvin/model/input/uppaal`;
 2. run the project under the current configuration.

Then, you will find the resulting ASF platform containing the target network/application model in the folder 
`../marvin/model/output/asf`. 

You can deploy the ASF platform and then run all the simulations you want.
Please note that, by using the attack description language provided by ASF, you can describe and simulate 
a number of attack scenarios, in order to evaluate the behavior of the target system in those scenarios. 

### Samples

For your convenience, the folder `../marvin/sample` contains input/output samples:
 - `../marvin/sample/input/uppaal` contains a sample UPPAAL model;
 - `../marvin/sample/output/asf` contains the ASF platform that already bundles the target network/application model.
 
### Deployment

If you don't use Ubuntu as development system, you can run it as a guest system on your host system through 
an hypervisor such as [VirtualBox](https://www.virtualbox.org/wiki/Downloads). 
For doing so, please refer the official user manual of the hypervisor you're using. 

You have to install OMNeT++ 4.6 on Ubuntu before building and running any simulation on ASF. 
For installing it, please refer the official [ASF manual](xxx).
Note that this project comes bundled with an image of ASF 1.1, 
which cannot run on OMNeT++ 5 or greater. 

On Ubuntu, choose a location where put the ASF image produced by Marvin.
Then, build and run it as it was a vanilla ASF image. 
For doing so, please, refer the official 
[ASF user manual](xxx). 

## Built With

### Development and testing environment

* macOS High Sierra 10.13.6
* [PyCharm](https://www.jetbrains.com/pycharm/) 2019.2.6 CE
* [Python](https://www.python.org/downloads/) 3.8.3
* [UPPAAL](http://www.uppaal.org/) 4.0.15

### Deployment environment

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads) 6.1.10
* [Ubuntu](https://ubuntu.com/) 18.04
* [OMNeT++](https://omnetpp.org/) 4.6

## Contributing

Please read our [contributing instructions](CONTRIBUTING.md) and our [code of conduct](CODE_OF_CONDUCT.md),
for details on the process of submitting requests to us.

## Versioning

We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Author

Francesco Racciatti

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) for details.

## Acknowledgments

* Maurizio Palmieri
* Cinzia Bernardeschi
* Gianluca Dini
